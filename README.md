Una collección de spinnets para simplicicarte la vida cuando escribas o empieces a generar tu código
en openerp.

Guarda los spinnets en el directorio: /home/usuario/.config/sublime-text-3/Packages/User

Abre archivos con extensiones *.py o *.xml y preciona Ctrl+Shift+p y pones el nombre del
snippet y te rellanera con secciones de código de acuerdo al snippet que escribas.

Tambien puedes crear tus propios snippets es muy sencillo:

Tools > New Snippet...

Espero les sirva Gracias!!!.
